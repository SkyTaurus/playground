(function ($) {
	$.Redactor.prototype.modal = function () {
		return {
			callbacks: {},
			loadTemplates: function () {
				this.opts.modal = {
					imageEdit: String()
					+ '<section id="redactor-modal-image-edit">'
					+ '<label>' + this.lang.get('title') + '</label>'
					+ '<input type="text" id="redactor-image-title" />'
					+ '<label class="redactor-image-link-option">' + this.lang.get('link') + '</label>'
					+ '<input type="text" id="redactor-image-link" class="redactor-image-link-option" aria-label="' + this.lang.get('link') + '" />'
					+ '<label class="redactor-image-link-option"><input type="checkbox" id="redactor-image-link-blank" aria-label="' + this.lang.get('link_new_tab') + '"> ' + this.lang.get('link_new_tab') + '</label>'
					+ '<label class="redactor-image-position-option">' + this.lang.get('image_position') + '</label>'
					+ '<select class="redactor-image-position-option" id="redactor-image-align" aria-label="' + this.lang.get('image_position') + '">'
					+ '<option value="none">' + this.lang.get('none') + '</option>'
					+ '<option value="left">' + this.lang.get('left') + '</option>'
					+ '<option value="center">' + this.lang.get('center') + '</option>'
					+ '<option value="right">' + this.lang.get('right') + '</option>'
					+ '</select>'
					+ '</section>',

					image: String()
					+ '<section id="redactor-modal-image-insert">'
					+ '<div id="redactor-modal-image-droparea"></div>'
					+ '</section>',

					file: String()
					+ '<section id="redactor-modal-file-insert">'
					+ '<div id="redactor-modal-file-upload-box">'
					+ '<label>' + this.lang.get('filename') + '</label>'
					+ '<input type="text" id="redactor-filename" aria-label="' + this.lang.get('filename') + '" /><br><br>'
					+ '<div id="redactor-modal-file-upload"></div>'
					+ '</div>'
					+ '</section>',

					link: String()
					+ '<section id="redactor-modal-link-insert">'
					+ '<label>URL</label>'
					+ '<input type="url" id="redactor-link-url" aria-label="URL" />'
					+ '<label>' + this.lang.get('text') + '</label>'
					+ '<input type="text" id="redactor-link-url-text" aria-label="' + this.lang.get('text') + '" />'
					+ '<label class="hide"><input type="checkbox" checked="checked" id="redactor-link-blank"> ' + this.lang.get('link_new_tab') + '</label>'
					+ '</section>'
				};


				$.extend(this.opts, this.opts.modal);

			},
			addCallback: function (name, callback) {
				this.modal.callbacks[name] = callback;
			},
			createTabber: function ($modal) {
				this.modal.$tabber = $('<div>').attr('id', 'redactor-modal-tabber');

				$modal.prepend(this.modal.$tabber);
			},
			addTab: function (id, name, active) {
				var $tab = $('<a href="#" rel="tab' + id + '">').text(name);
				if (active) {
					$tab.addClass('active');
				}

				var self = this;
				$tab.on('click', function (e) {
					e.preventDefault();
					$('.redactor-tab').hide();
					$('.redactor-' + $(this).attr('rel')).show();

					self.modal.$tabber.find('a').removeClass('active');
					$(this).addClass('active');

				});

				this.modal.$tabber.append($tab);
			},
			addTemplate: function (name, template) {
				this.opts.modal[name] = template;
			},
			getTemplate: function (name) {
				return this.opts.modal[name];
			},
			getModal: function () {
				return this.$modalBody.find('section');
			},
			load: function (templateName, title, width) {
				this.modal.templateName = templateName;
				this.modal.width = width;

				this.modal.build();
				this.modal.enableEvents();
				this.modal.setTitle(title);
				this.modal.setDraggable();
				this.modal.setContent();

				// callbacks
				if (typeof this.modal.callbacks[templateName] != 'undefined') {
					this.modal.callbacks[templateName].call(this);
				}

			},
			show: function () {
				this.utils.disableBodyScroll();

				if (this.utils.isMobile()) {
					this.modal.showOnMobile();
				}
				else {
					this.modal.showOnDesktop();
				}

				if (this.opts.highContrast) {
					this.$modalBox.addClass("redactor-modal-contrast");
				}

				this.$modalOverlay.show();
				this.$modalBox.show();

				this.$modal.attr('tabindex', '-1');
				this.$modal.focus();

				this.modal.setButtonsWidth();

				this.utils.saveScroll();

				// resize
				if (!this.utils.isMobile()) {
					setTimeout($.proxy(this.modal.showOnDesktop, this), 0);
					$(window).on('resize.redactor-modal', $.proxy(this.modal.resize, this));
				}

				// modal shown callback
				this.core.setCallback('modalOpened', this.modal.templateName, this.$modal);

				// fix bootstrap modal focus
				$(document).off('focusin.modal');

				// enter
				this.$modal.find('input[type=text],input[type=url],input[type=email]').on('keydown.redactor-modal', $.proxy(this.modal.setEnter, this));
			},
			showOnDesktop: function () {
				var height = this.$modal.outerHeight();
				var windowHeight = $(window).height();
				var windowWidth = $(window).width();

				if (this.modal.width > windowWidth) {
					this.$modal.css({
						width: '96%',
						marginTop: (windowHeight / 2 - height / 2) + 'px'
					});
					return;
				}

				if (height > windowHeight) {
					this.$modal.css({
						width: this.modal.width + 'px',
						marginTop: '20px'
					});
				}
				else {
					this.$modal.css({
						width: this.modal.width + 'px',
						marginTop: (windowHeight / 2 - height / 2) + 'px'
					});
				}
			},
			showOnMobile: function () {
				this.$modal.css({
					width: '96%',
					marginTop: '2%'
				});

			},
			resize: function () {
				if (this.utils.isMobile()) {
					this.modal.showOnMobile();
				}
				else {
					this.modal.showOnDesktop();
				}
			},
			setTitle: function (title) {
				this.$modalHeader.html(title);
			},
			setContent: function () {
				this.$modalBody.html(this.modal.getTemplate(this.modal.templateName));
			},
			setDraggable: function () {
				if (typeof $.fn.draggable === 'undefined') return;

				this.$modal.draggable({handle: this.$modalHeader});
				this.$modalHeader.css('cursor', 'move');
			},
			setEnter: function (e) {
				if (e.which != 13) return;

				e.preventDefault();
				this.$modal.find('button.redactor-modal-action-btn').click();
			},
			createCancelButton: function () {
				var button = $('<button>').addClass('redactor-modal-btn redactor-modal-close-btn').html(this.lang.get('cancel'));
				button.on('click', $.proxy(this.modal.close, this));

				this.$modalFooter.append(button);
			},
			createDeleteButton: function (label) {
				return this.modal.createButton(label, 'delete');
			},
			createActionButton: function (label) {
				return this.modal.createButton(label, 'action');
			},
			createButton: function (label, className) {
				var button = $('<button>').addClass('redactor-modal-btn').addClass('redactor-modal-' + className + '-btn').html(label);
				this.$modalFooter.append(button);

				return button;
			},
			setButtonsWidth: function () {
				var buttons = this.$modalFooter.find('button');
				var buttonsSize = buttons.length;
				if (buttonsSize === 0) return;

				buttons.css('width', (100 / buttonsSize) + '%');
			},
			build: function () {
				this.modal.buildOverlay();

				this.$modalBox = $('<div id="redactor-modal-box"/>').hide();
				this.$modal = $('<div id="redactor-modal" role="dialog" aria-labelledby="redactor-modal-header" />');
				this.$modalHeader = $('<header id="redactor-modal-header"/>');
				this.$modalClose = $('<button type="button" id="redactor-modal-close" tabindex="1" aria-label="Close" />').html('&times;');
				this.$modalBody = $('<div id="redactor-modal-body" />');
				this.$modalFooter = $('<footer />');

				this.$modal.append(this.$modalHeader);
				this.$modal.append(this.$modalClose);
				this.$modal.append(this.$modalBody);
				this.$modal.append(this.$modalFooter);
				this.$modalBox.append(this.$modal);
				this.$modalBox.appendTo(document.body);
			},
			buildOverlay: function () {
				this.$modalOverlay = $('<div id="redactor-modal-overlay">').hide();
				$('body').prepend(this.$modalOverlay);
			},
			enableEvents: function () {
				this.$modalClose.on('click.redactor-modal', $.proxy(this.modal.close, this));
				$(document).on('keyup.redactor-modal', $.proxy(this.modal.closeHandler, this));
				this.$editor.on('keyup.redactor-modal', $.proxy(this.modal.closeHandler, this));
				this.$modalBox.on('click.redactor-modal', $.proxy(this.modal.close, this));
			},
			disableEvents: function () {
				this.$modalClose.off('click.redactor-modal');
				$(document).off('keyup.redactor-modal');
				this.$editor.off('keyup.redactor-modal');
				this.$modalBox.off('click.redactor-modal');
				$(window).off('resize.redactor-modal');
			},
			closeHandler: function (e) {
				if (e.which != this.keyCode.ESC) return;

				this.modal.close(false);
			},
			close: function (e) {
				if (e) {
					if (!$(e.target).hasClass('redactor-modal-close-btn') && e.target != this.$modalClose[0] && e.target != this.$modalBox[0]) {
						return;
					}

					e.preventDefault();
				}

				if (!this.$modalBox) return;

				this.modal.disableEvents();
				this.utils.enableBodyScroll();

				this.$modalOverlay.remove();

				this.$modalBox.fadeOut('fast', $.proxy(function () {
					this.$modalBox.remove();

					setTimeout($.proxy(this.utils.restoreScroll, this), 0);

					if (e !== undefined) this.selection.restore();

					$(document.body).css('overflow', this.modal.bodyOveflow);
					this.core.setCallback('modalClosed', this.modal.templateName);

				}, this));

			}
		};
	}
})(jQuery);
