<?php

class LngSourceMessages extends ActiveRecord
{
	public function tableName()
	{
		return 'lng_source_messages';
	}

	public function relations()
	{
		return array(
			'idTranslated' => array(self::HAS_ONE, 'LngTranslatedMessages', ['id'=>'id']),
		);
	}

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public static function genHash($str)
	{
		return md5($str);
	}
}
