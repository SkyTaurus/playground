<?php
class ProfileSolo extends ActiveRecord
{
	const CLICKS_MIN = 50; //100
	const CLICKS_MAX = 1000; // maximum clicks in order
	const CLICKS_MAX_ULTRA = 3000; // maximum clicks in order
	const CLICKS_MAX_DAILY = 10000;
	const CLICKS_STEP = 25;
	const CLICKS_STEP_SERACH = 100;

	const PRICE_MIN = 0.35;//0.3
	const PRICE_MAX = 0.95;//1.00;
	const PRICE_STEP = 0.01;

	const PRICE_SPECIAL_MAX = 5.00;//1.00;
	const PRICE_SPECIAL_STEP = 0.05;

	const KENT_PERCENT_MIN = 5;
	const KENT_PERCENT_MAX = 80;
	const KENT_PERCENT_STEP = 1;

	const OVERDELIVERY_PERCENT_MAX = 50;

	// Задержка. Используется при расчете минимально допустимого времени отправки.
	// Один час зазор между дедлайном принятия и send time и
	// один час компенсации незавершенного часа. Итого 2 часа.
	const SEND_TIME_DELAY = 0; // hours

	// Время реакции на предложение для продавца. Неуспел принять - expired.
	// Если время отправки наступает менее чем через 24 часа (относительно текущего момента), то
	// время реакции продавцу устанавливается равным order_ttl (значение из settings продавца).
	const ORDER_TTL_MIN = 5; // hours
	const ORDER_TTL_MAX = 24; // hours

	// Если время отправки наступает более 24 часов (относительно текущего момента), то
	// время реакции продавцу устанавливается равным ORDER_TTL_OLDEST.
	const ORDER_TTL_OLDEST = 24; // hours

	/**
	 * 'временная переменная' для работы с locked_week_days
	 */
	public $__locked_week_days;
	public $__locked_week_days_old;
	public $locked_week_days = array();

	public $__best_solo_time;

	const HOT_SALE_PERIOD_DAYS = 7;
	const HOT_SALE_MINIMUM = 6;

	const FLOAT_PRICE_LIMIT_DAYS = 7;

	const TIMEZONE_LOCAL = 'loc';
	const TIMEZONE_UTC = 'utc';
	const TIMEZONE_EST = 'est';

	/**
	 * @var boolean
	 */
	public $is_locked_day;

	/**
	 * @var date (yyyy-mm-dd)
	 */
	public $locked_day;

	public $sell_days = array();

	public $old_is_active;

	public $seller_tags;
	public $sellerTagsModels;

	public $order_urgency;

	/*public function behaviors()
	{
		return CMap::mergeArray(parent::behaviors(), array());
	}*/

	public function tableName()
	{
		return 'profile_solo';
	}

	public function rules()
	{
		return array(
			// general settings
			array('is_active', 'boolean', 'on'=>'settingsSellerGeneral'),

			array('clicks_max, clicks_min', 'required', 'on'=>'settingsSellerGeneral'),
			array('clicks_max, clicks_min', 'clicksValidator', 'on'=>'settingsSellerGeneral'),
			array('clicks_max', 'compare', 'compareAttribute'=>'clicks_min', 'operator'=>'>=',
				'on'=>'settingsSellerGeneral'),

			array('clicks_max_daily', 'required', 'on'=>'settingsSellerGeneral'),
			array('clicks_max_daily', 'clicksDailyValidator', 'on'=>'settingsSellerGeneral'),
			array('clicks_max_daily', 'compare', 'compareAttribute'=>'clicks_max', 'operator'=>'>=',
				'on'=>'settingsSellerGeneral'),

			array('click_price_max, click_price_min', 'required', 'on'=>'settingsSellerGeneral'),
			array('click_price_max, click_price_min', 'clickPriceValidator', 'on'=>'settingsSellerGeneral'),
			array('click_price_max', 'compare', 'compareAttribute'=>'click_price_min', 'operator'=>'>=',
				'on'=>'settingsSellerGeneral'),

			array('best_solo_timezone', 'in', 'range' => array_keys(self::bestSoloTimezoneOptions()), 'allowEmpty' => true, 'on'=>'settingsSellerGeneral'),

			array('locked_day', 'type', 'type' => 'date', 'dateFormat' => 'yyyy-MM-dd', 'on'=>'lockday,settingsSellerGeneral'),
			array('is_locked_day', 'boolean', 'on'=>'lockday,settingsSellerGeneral'),
			array('__locked_week_days', 'sellDaysValidator', 'on'=>'lockday,settingsSellerGeneral'),
			array('solos_number', 'solosNumberValidator', 'on'=>'lockday,settingsSellerGeneral'),
			array('order_urgency', 'orderUrgencyValidator', 'on'=>'lockday,settingsSellerGeneral'),

			array('__best_solo_time', 'bestSoloTimeValidator', 'on'=>'settingsSellerGeneral'),

			// control
			array('is_baned', 'boolean', 'on'=>'control-edit'),

			array('before_order_form', 'filter', 'filter' => function ($val) {
					$obj = new MyHtmlPurifier();
					return $obj->purifyUserText($val);
				},
				'on' => 'profile-edit, before_order_form'
			),
			array('before_order_form', 'length', 'max'=>4000, 'on'=>'profile-edit, before_order_form'),
			array('after_order_form', 'filter', 'filter' => function ($val) {
				$obj = new MyHtmlPurifier();
				return $obj->purifyUserText($val);
			},
				'on' => 'profile-edit, after_order_form'
			),
			array('after_order_form', 'length', 'max'=>4000, 'on'=>'profile-edit, after_order_form'),

			array('seller_tags', 'filter', 'filter'=>'trim', 'on'=>'profile-edit'),
			array('seller_tags', 'length', 'max'=>1000, 'on'=>'profile-edit'),
			array('seller_tags', 'match', 'pattern'=>'/^[\w\s,]+$/', 'message'=>'Profile tags should contain only english letters', 'on'=>'profile-edit'),
			array('seller_tags', 'sellerTagsValidator', 'on'=>'profile-edit'),
		);
	}

	public function clicksValidator($attribute, $params)
	{
		$arr = self::clicksOptions();
		if (!in_array($this->$attribute, array_keys($arr))) {
			$this->addError($attribute, 'Field "'.$this->getAttributeLabel($attribute).'" has wrong value.');
		}
	}

	public function clicksDailyValidator($attribute, $params)
	{
		$arr = self::clicksDailyOptions();
		if (!in_array($this->$attribute, array_keys($arr))) {
			$this->addError($attribute, 'Field "'.$this->getAttributeLabel($attribute).'" has wrong value.');
		}
	}

	public function orderUrgencyValidator($attribute, $params)
	{
		$arr = self::orderUrgencyOptions();
		if (!in_array($this->$attribute, array_keys($arr))) {
			$this->addError($attribute, 'Field "'.$this->getAttributeLabel($attribute).'" has wrong value.');
		}
	}

	public function clickPriceValidator($attribute, $params)
	{
		$arr = self::clickPriceOptions();
		if (!in_array($this->$attribute, array_keys($arr))) {
			$this->addError($attribute, 'Field "'.$this->getAttributeLabel($attribute).'" has wrong value.');
		}
	}

	public function sellDaysValidator($attribute, $params)
	{
		if (!empty($this->$attribute)) {
			$arr = self::sellDaysOptions();
			foreach ($this->$attribute as $item) {
				if (!in_array($item, array_keys($arr))) {
					$this->addError($attribute, 'Field "'.$this->getAttributeLabel($attribute).'" has wrong value.');
					break;
				}
			}
			if (sizeof($this->$attribute) >= 7) {
				$this->__locked_week_days = $this->__locked_week_days_old;
				$this->addError($attribute, 'You must have available at least one week day. If you wish to stop selling, turn off the Seller mode.');
			}
		}
	}

	public function bestSoloTimeValidator($attribute, $params)
	{
		if (!empty($this->$attribute) && is_array($this->$attribute)) {
			foreach ($this->$attribute as $item) {
				$item = $item+0;
				if (!is_int($item) || $item<0 || $item>23) {
					$this->addError($attribute, 'Field "'.$this->getAttributeLabel($attribute).'" has wrong value.');
					break;
				}
			}
		} else {
			$this->$attribute = [];
		}
	}

	public function solosNumberValidator($attribute)
	{
		$arr = self::solosNumberOptions();
		$keys = array_keys($arr);

		foreach ($keys as $key) {
			if ($key.'' == $this->$attribute.'') {
				return true;
			}
		}
		$this->addError($attribute, 'Field "'.$this->getAttributeLabel($attribute).'" has wrong value.');
	}

	public function sellerTagsValidator($attribute)
	{
		if (!empty($this->$attribute)) {
			$arr = preg_split('/\s*,\s*/',trim($this->$attribute),-1,PREG_SPLIT_NO_EMPTY);
			$arr = array_unique($arr);
			$this->seller_tags = implode(', ', $arr);
			foreach ($arr as $tag) {
				$nModel = new SellerTags();
				$nModel->id_user = Yii::app()->user->id;
				$nModel->tag = trim($tag);
				$this->sellerTagsModels[] = $nModel;
			}
		} else {
			$this->sellerTagsModels = [];
		}
	}

	public function relations()
	{
		return array(
			'idUser' => array(self::BELONGS_TO, 'Users', 'id_user'),
		);
	}

	public function attributeLabels()
	{
		return array(
			'order_urgency' => 'Order urgency',
			'clicks_min' => 'Order quantity',
			'clicks_max' => 'Order quantity',
			'clicks_max_daily' => 'Daily max',
			'click_price' => 'Price per click',
			'is_active' => 'Seller mode',
			'is_baned' => 'Ban seller',
			'is_kent'=>'Affiliate program',
			'kent_percent'=>'Pay from order total',
			'kent_note'=>'Note for affiliates',
			'extra_price_swipe'=>'Ad copy',
			'extra_price_squeeze'=>'Landing page',
			'extra_price_custom_name1'=>"Name for Other",
			'extra_price_custom_value1'=>'Custom extra price',
			'solos_number'=>'Maximum orders',
			'before_order_form'=>'Text to show before order form',
			'after_order_form'=>'Text to show after order form',
			'best_solo_timezone'=>'Best order hours',
			'search_handicap'=>'Search handicap',
		);
	}

	public function setAttribute($name,$value)
	{
		if (parent::setAttribute($name,$value)) {
			if ($name == 'locked_day' && ($this->scenario=='lockday' || $this->scenario=='settingsSellerGeneral')) {
				if (in_array($value, $this->locked_days)) {
					$this->is_locked_day = 1;
				} else {
					$this->is_locked_day = 0;
				}
			}
			return true;
		} else {
			return false;
		}
	}

	protected function afterFind()
	{
		if ($this->sell_sun)
			$this->sell_days[] = 0;
		else
			$this->locked_week_days[] = 0;

		if ($this->sell_mon)
			$this->sell_days[] = 1;
		else
			$this->locked_week_days[] = 1;

		if ($this->sell_tue)
			$this->sell_days[] = 2;
		else
			$this->locked_week_days[] = 2;

		if ($this->sell_wed)
			$this->sell_days[] = 3;
		else
			$this->locked_week_days[] = 3;

		if ($this->sell_thu)
			$this->sell_days[] = 4;
		else
			$this->locked_week_days[] = 4;

		if ($this->sell_fri)
			$this->sell_days[] = 5;
		else
			$this->locked_week_days[] = 5;

		if ($this->sell_sat)
			$this->sell_days[] = 6;
		else
			$this->locked_week_days[] = 6;


		if (!empty($this->locked_week_days)) {
			$this->__locked_week_days = $this->locked_week_days;
		} else {
			$this->__locked_week_days = array();
		}

		$this->__locked_week_days_old = $this->__locked_week_days;

		if (!empty($this->locked_days)) {
			$this->locked_days = unserialize($this->locked_days);
		} else {
			$this->locked_days = array(1);
		}

		// best_solo_time
		if (!empty($this->best_solo_time)) {
			$this->__best_solo_time = CJSON::decode($this->best_solo_time);
		} else {
			$this->__best_solo_time = [];
		}

		$this->old_is_active = $this->is_active;

		if ($this->order_delay) {
			$this->order_urgency = $this->order_delay*24;
		} else {
			$this->order_urgency = $this->order_ttl;
		}

		parent::afterFind();
	}

	protected function beforeSave()
	{

		if (parent::beforeSave()) {

			if ($this->isNewRecord) {
				//clicks
				$clicks = $this->clicksOptions();
				$this->clicks_min = key($clicks);
				next($clicks);
				$this->clicks_max = empty($clicks[300]) ? key($clicks) : 300;
				$this->clicks_max_daily = $this->clicks_max;

				//price
				$price = $this->clickPriceOptions();
				$this->click_price = $this->click_price_min = $this->click_price_max = key($price);

				//sell days
				$this->sell_mon = $this->sell_tue = $this->sell_wed = $this->sell_thu = $this->sell_fri = $this->sell_sat = $this->sell_sun = 1;

				$this->order_ttl = self::ORDER_TTL_MAX;
				$this->order_delay = 3;

				$this->is_active = 0;
				$this->solos_number = 1;
				$this->is_kent = 1;
			}

			if (in_array($this->scenario, array('settingsSellerExtraPrices'))) {
				$this->extra_price_swipe = empty($this->extra_price_swipe) ? new CDbExpression('null') : $this->extra_price_swipe;
				$this->extra_price_squeeze = empty($this->extra_price_squeeze) ? new CDbExpression('null') : $this->extra_price_squeeze;
				$this->extra_price_custom_value1 = empty($this->extra_price_custom_value1) ? new CDbExpression('null') : $this->extra_price_custom_value1;
			}

			if ($this->scenario == 'lockday' || $this->scenario == 'settingsSellerGeneral') {
				$this->lockWeekDays();
				$this->lockSingleDay();
			}

			if (is_array($this->locked_days)) {
				if (empty($this->locked_days)) {
					$this->locked_days = '';
				} else {
					$this->locked_days = serialize($this->locked_days);
				}
			}

			if ($this->scenario == 'settingsSellerGeneral') {
				if (is_array($this->__best_solo_time)) {
					$this->best_solo_time = CJSON::encode($this->__best_solo_time);
				}
			}

			if ($this->scenario == 'settingsSellerGeneral') {
				if ($this->order_urgency<=self::ORDER_TTL_MAX) {
					$this->order_ttl = $this->order_urgency;
					$this->order_delay = 0;
				} else {
					$this->order_ttl = self::ORDER_TTL_MAX;
					$this->order_delay = $this->order_urgency/24;
				}
			}

			return true;
		} else {
			return false;
		}
	}

	protected function afterSave()
	{
		if (in_array($this->scenario, array('settingsSellerExtraPrices'))) {
			$this->extra_price_swipe = ($this->extra_price_swipe instanceof CDbExpression && $this->extra_price_swipe->expression == 'null') ? null : $this->extra_price_swipe;
			$this->extra_price_squeeze = ($this->extra_price_squeeze instanceof CDbExpression && $this->extra_price_squeeze->expression == 'null') ? null : $this->extra_price_squeeze;
			$this->extra_price_custom_value1 = ($this->extra_price_custom_value1 instanceof CDbExpression && $this->extra_price_custom_value1->expression == 'null') ? null : $this->extra_price_custom_value1;
		}

		if ($this->sellerTagsModels !== null) {
			SellerTags::clearAll(Yii::app()->user->id);
			foreach ($this->sellerTagsModels as $model) {
				$model->save(false);
			}
		}

		parent::afterSave();
	}

	public function scopes()
	{
		return array(
			'curUser'	=> array(
				'condition'	=> 't.id_user=:id_user',
				'params' => array(
					':id_user'	=> Yii::app()->user->id
				),
			),
		);
	}

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public static function clickPriceOptions($min=false, $max=false)
	{
		if (!$min) $min = self::PRICE_MIN;
		if (!$max) $max = self::PRICE_MAX;

		$result = array();
		for($i=$min; $i<=$max+0.01; $i=$i+($i<1 ? self::PRICE_STEP : self::PRICE_SPECIAL_STEP)) {
			$result[number_format($i, 2)] = '$'.number_format($i, 2);
		}
		return $result;
	}

	public static function bestSoloTimezoneOptions()
	{
		return [
			self::TIMEZONE_UTC=>'UTC',
			self::TIMEZONE_EST=>'EST',
			self::TIMEZONE_LOCAL=>'Local',
		];
	}

	public static function sellDaysOptions($longNames = false)
	{
		if (!$longNames) {
			return array(
				'1' => 'M',
				'2' => 'T',
				'3' => 'W',
				'4' => 'T',
				'5' => 'F',
				'6' => 'S',
				'0' => 'S',
			);
		} else {
			return array(
				'1' => 'Mon',
				'2' => 'Tue',
				'3' => 'Wed',
				'4' => 'Thu',
				'5' => 'Fri',
				'6' => 'Sat',
				'0' => 'Sun',
			);
		}

	}

	public static function orderUrgency()
	{
		$result = [];
		for($i=self::ORDER_TTL_MIN; $i<=self::ORDER_TTL_MAX; $i++) {
			$result[$i] = array(
				'title' => Yii::t('app', '{n} hour|{n} hours', $i),
				'hint' => 'You will have <span class="red-color">'.Yii::t('app', '{n} hour|{n} hours', $i).'</span> to accept new orders.',
			);
		}

		for($i=2; $i<=14; $i++) {
			$result[$i*24] = array(
				'title' => Yii::t('app', '{n} day|{n} days', $i),
				'hint' => 'You will have '.Yii::t('app', '{n} hour|{n} hours', self::ORDER_TTL_OLDEST).' to accept new orders.',
			);
		}

		return $result;
	}

	public static function orderUrgencyOptions()
	{
		$result = array();
		foreach (self::orderUrgency() as $key=>$value) {
			$result[$key] = $value['title'];
		}
		return $result;
	}

	public static function isActiveOptions()
	{
		return array(
			'1'=>'ON',
			'0'=>'OFF',
		);
	}

	public static function solosNumberOptions()
	{
		return array(
			'1-3'=>'1 solo in 3 days',
			'1-2'=>'1 solo in 2 days',
			'1'=>'1 solo per day',
			'2'=>'2 solos per day',
			'3'=>'3 solos per day',
			'4'=>'4 solos per day',
			'5'=>'5 solos per day',
			'unlimited'=>'unlimited',
		);
	}

	/*
	 * limit for 1 solo
	 */
	public static function clicksOptions($min=false, $max=false, $is_search=false, $tpl='{val}')
	{
		if (!$min) $min = self::CLICKS_MIN;
		if (!$max) $max = self::CLICKS_MAX;

		$result = [];
		for($i=$min; $i<=$max; $i=$i+$step) {
			$result[$i] = str_replace('{val}', $i, $tpl);

			if ($i>=100 && $is_search) $step = self::CLICKS_STEP_SERACH;
			else $step = self::CLICKS_STEP;
		}

		return $result;
	}

	public static function clicksDailyOptions($tpl='{val}')
	{
		$result = [];
		for ($i=self::CLICKS_MIN; $i<=self::CLICKS_MAX_DAILY; $i=$i+$step) {
			$result[$i] = str_replace('{val}', $i, $tpl);
			$step = self::CLICKS_STEP;
		}

		return $result;
	}

	public static function kentPercentOptions($min=false, $max=false)
	{
		if (!$min) {
			$min = self::KENT_PERCENT_MIN;
		}
		if (!$max) {
			$max = self::KENT_PERCENT_MAX;
		}
		$result = array();
		for($i=$min; $i<=$max; $i=$i+self::KENT_PERCENT_STEP) {
			$result[$i] = $i.'%';
		}
		return $result;
	}

	private function lockWeekDays()
	{
		$this->sell_mon = $this->sell_tue = $this->sell_wed = $this->sell_thu = $this->sell_fri = $this->sell_sat = $this->sell_sun = 1;
		if (!empty($this->__locked_week_days)) {
			$days = array('sell_sun', 'sell_mon', 'sell_tue', 'sell_wed', 'sell_thu', 'sell_fri', 'sell_sat');
			foreach ($this->__locked_week_days as $v) {
				$this->$days[$v] = 0;
			}
		}
	}

	private function lockSingleDay()
	{
		// удалим прошедшие даты
		if (!empty($this->locked_days)) {
			$arr = $this->locked_days;
			foreach ($this->locked_days as $key=>$date) {
				if (strtotime($date) < strtotime(date('Y-m-d'))) {
					unset($arr[$key]);
					$this->setAttribute('locked_days', $arr);
				}
			}
		}

		if (!$this->is_locked_day) {
			// если лочим, то добавляем день
			if (!in_array($this->locked_day, $this->locked_days)) {
				$arr = $this->locked_days;
				$arr[] = $this->locked_day;
				$this->setAttribute('locked_days', $arr);
			}
		} else {
			// если разлочиваем, то удаляем день
			$key = array_search($this->locked_day, $this->locked_days);
			if (!is_null($key) && $key !== false) {
				$arr = $this->locked_days;
				unset($arr[$key]);
				$this->setAttribute('locked_days', $arr);
			}
		}
	}

	// Пересчет плавающей цены исходя из загрузки календаря только реальными заказами.
	 // Прочие локинги не учитываются.
	public function updateClickPrice()
	{
		if ($this->click_price_min != $this->click_price_max) {

			$employmentCount = 0;

			$click_price = round($this->click_price_min + ($this->click_price_max - $this->click_price_min) * (1 - (self::FLOAT_PRICE_LIMIT_DAYS - $employmentCount) / self::FLOAT_PRICE_LIMIT_DAYS), 2);

			$this->updateByPk($this->id, ['click_price'=>$click_price]);
		}
	}

	public function getOrderTotalBuyerPrice($orderClicks)
	{
		return ceil($this->click_price * $orderClicks + Solos::COMMISSION_CLICK_FILTER);
	}

	public function getSalesPercent()
	{
		return round($this->sales_cnt_total/Ratings::GOT_SALES_SOLOS_LIMIT, 2)*100;
	}

	public function getBestSoloTime($options)
	{
		if (!empty($options) && !empty($this->__best_solo_time)) {
			$best = $this->__best_solo_time;
			foreach ($best as $key=>$val) {
				$best[$key] = str_pad($val, 2, '0', STR_PAD_LEFT).':00:00';
			}
			foreach ($options as $key=>$val) {
				if (in_array($key, $best)) {
					return $key;
				}
			}
			return '';
		} else {
			return '';
		}
	}
}
