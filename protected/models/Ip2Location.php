<?php
class Ip2Location extends ActiveRecord
{
	const EARTH_RADIUS = 6378137;

	public function tableName()
	{
		return 'ip2_location';
	}

	public function rules()
	{
		return array(
		);
	}

	public function relations()
	{
		return array(
		);
	}

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public static function selectInfoLocation($ip=false)
	{
		$ip = $ip ? $ip : (empty($_SERVER['REMOTE_ADDR']) ? '' : $_SERVER['REMOTE_ADDR']);
		$sql = "SELECT * FROM
				(SELECT * FROM ip2_location WHERE ipFROM <=INET_ATON(:ip) ORDER BY ipFROM DESC LIMIT 1) AS t
				WHERE ipTO >=INET_ATON(:ip)
			";
		$command = Yii::app()->db->createCommand($sql);
		$command->bindValue(':ip', $ip);
		$row = $command->queryRow();
		if (!$row) $row = Ip2Location::model()->attributes;
		return $row;
	}

	public static function getUsageTypeDescription($usage_type)
	{
		switch ($usage_type) {
			case 'COM':
				$description = 'Commercial';
				break;
			case 'ORG':
				$description = 'Organization';
				break;
			case 'GOV':
				$description = 'Government';
//				$description = '<span class="red-color">Content delivery network</span>';
				break;
			case 'MIL':
				$description = 'Military';
				break;
			case 'EDU':
				$description = 'University/College';
				break;
			case 'LIB':
				$description = 'Library';
				break;
			case 'CDN':
				$description = 'Content delivery network';
				break;
			case 'ISP':
				$description = '<span class="icon-home-click"></span> <span class="green-color">Home</span>';
				break;
			case 'MOB':
				$description = 'Mobile';
				break;
			case 'ISP/MOB':
				$description = '<span class="icon-home-click"></span> <span class="green-color">Home/Mobile</span>';
				break;
			case 'DCH':
				$description = 'Data center/Web';
				break;
			case 'Hosting/Transit':
				$description = 'Hosting/Transit';
				break;
			case 'SES':
				$description = 'Search engine spider';
				break;
			case 'RSV':
				$description = 'Reserved';
				break;
			default:
				$description = $usage_type;
		}
		return $description;
	}

	public static function getDistance($lat1, $lng1, $lat2, $lng2)
	{
		$lat1 = deg2rad($lat1);
		$lng1 = deg2rad($lng1);
		$lat2 = deg2rad($lat2);
		$lng2 = deg2rad($lng2);
		return round(self::EARTH_RADIUS * acos(cos($lat1) * cos($lat2) * cos($lng2 - $lng1) + sin($lat1) * sin($lat2)));
	}
}