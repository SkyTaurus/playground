<?php

class NicheUsers extends ActiveRecord
{
	const NICHES_MAX = 3;

	public function tableName()
	{
		return 'niche_users';
	}

	public function rules()
	{
		return array(
		);
	}

	public function relations()
	{
		return array(
			'idUser' => array(self::BELONGS_TO, 'Users', 'id_user'),
			'idNiche' => array(self::BELONGS_TO, 'Niches', 'id_niche'),
		);
	}

	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_user' => 'Id User',
			'id_niche' => 'Id Niche',
		);
	}

	public function scopes()
	{
		return array(
			'curUser'	=> array(
				'condition'	=> 't.id_user=:id_user',
				'params' => array(
					':id_user'	=> Yii::app()->user->id
				),
			),
		);
	}


	protected function beforeSave()
	{
		if (parent::beforeSave()) {
			if ($this->scenario == 'settings')
				$this->id_user = Yii::app()->user->id;
			return true;
		} else {
			return false;
		}
	}

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
