<?php
class EmailBlacklist extends ActiveRecord
{
	public function tableName()
	{
		return 'email_blacklist';
	}

	public function rules()
	{
		return array(
		);
	}

	public function relations()
	{
		return array(
			'idUser' => array(self::BELONGS_TO, 'Users', 'id_user'),
		);
	}

	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'email' => 'Email',
			'id_user' => 'Id User',
			'status' => 'Status',
			'create_dta' => 'Create Dta',
		);
	}

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
