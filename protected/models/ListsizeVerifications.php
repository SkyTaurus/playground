<?php
class ListsizeVerifications extends ActiveRecord
{
	const STATUS_NEW = 'new';
	const STATUS_COMPLETE = 'complete';

	const COMPLETE_RESULT_APPROVED = 'approved';
	const COMPLETE_RESULT_DENIED = 'denied';
	const COMPLETE_RESULT_CANCELED = 'canceled';

	const RESULT_CODE_AUTO = 'auto';
	const RESULT_CODE_MANUAL = 'manual';
	const RESULT_CODE_BOTH = 'both';
	const RESULT_CODE_ERROR = 'error';

	public $idsAppRequest;

	public function tableName()
	{
		return 'listsize_verifications';
	}

	public function relations()
	{
		return array(
			'idUser' => array(self::BELONGS_TO, 'Users', 'id_user'),
			'idApp' => array(self::BELONGS_TO, 'Apps', 'id_app'),
			'idAppUser' => array(self::BELONGS_TO, 'AppsUsers', 'id_app_user'),
		);
	}

	public function rules()
	{
		return array(
			array('idsAppRequest', 'specialValidator', 'on'=>'create_verification'),

			array('new_value', 'required', 'on'=>'control_custom_verify'),
			array('new_value', 'numerical', 'integerOnly'=>true, 'on'=>'control_custom_verify'),
			array('new_value', 'compare', 'compareValue'=>1, 'operator'=>'>=', 'message'=>'Verified list size is zero.', 'on'=>'control_custom_verify'),
			array('new_value', 'compare', 'compareValue'=>999999, 'operator'=>'<=', 'on'=>'control_custom_verify'),
			array('new_value', 'length', 'max'=>6, 'on'=>'control_custom_verify'),
		);
	}

	/**
	 * Верификация на предмет:
	 * 1. должно быть выбрано хотя бы одно приложение.
	 * 2. переданый id приложения должен существовать среди приложений.
	 * 3. выбранное приложение должно быть подключено.
	 * 4. по выбранному приложению не должно существовать уже отправленного (незавершенного) запроса на верификацию.
	 * @param $attribute
	 *
	 * @throws CException
	 */
	public function specialValidator($attribute)
	{
		if (!is_array($this->$attribute)) {
			throw new CException('not array attribute');
		}

		//#1
		$checkedValues = array();
		foreach ($this->$attribute as $id_app) {
			if (!empty($id_app)) {
				$checkedValues[] = $id_app;
			}
		}
		if (empty($checkedValues)) {
			$this->addError($attribute, 'Please select an autoresponder(s)');
			return;
		}

		//#2-#4
		$listIndexed = array();
		$modelsApp = Apps::model()->activeAutoresponders()->newRequests(Yii::app()->user->id)->findAll();
		foreach ($modelsApp as $modelApp) {
			$listIndexed[$modelApp->id] = $modelApp;
		}
		foreach ($checkedValues as $id_app) {

			if (empty($listIndexed[$id_app])) {
				$this->addError($attribute, 'Selected autoresponder(s) does not exists');
				return;
			}

			if (!empty($listIndexed[$id_app]->listsizeVerifications)) {
				$this->addError($attribute, 'Verification request already sent for "'.$listIndexed[$id_app]->name.'"');
				return;
			}

			$class = $listIndexed[$id_app]->class;
			$inst = new $class(Yii::app()->user->id);
			if (!$inst->isConnected()) {
				$this->addError($attribute, 'Application "'.$inst->getAppName().'" is not connected');
				return;
			}
		}

		$this->idsAppRequest = $checkedValues;
	}

	public function attributeLabels()
	{
		return array(
			'old_value' => 'Old Value',
			'new_value' => 'Verified list size',
			'create_dta' => 'Create Dta',
			'status' => 'Status',
			'complete_result' => 'Complete Result',
			'complete_dta' => 'Complete Dta',
			'admin_url' => 'Admin Url',
			'login' => 'Login',
			'password' => 'Password',
			'descr' => 'Descr',
		);
	}

	public function search()
	{
		$criteria=new CDbCriteria;
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function newRequest($id_user)
	{
		$this->getDbCriteria()->mergeWith(array(
			'condition'=>'(id_user = :id_user) AND (status = :status)',
			'params'=>array(':id_user'=>$id_user, ':status'=>self::STATUS_NEW),
			'order'=>'t.create_dta DESC',
		));
		return $this;
	}

	public static function createRequests($idsAppRequest)
	{
		$result = array();
		foreach ($idsAppRequest as $id_app) {
			if (empty($id_app)) {
				throw new CException('Validation missed');
			}
			$modelAppUser = AppsUsers::model()->with('idApp')->findByAttributes(array('id_app'=>$id_app, 'id_user'=>Yii::app()->user->id));
			if (empty($modelAppUser)) {
				throw new CException('model AppsUsers not found');
			}
			$inst = new ListsizeVerifications();
			$inst->id_user = Yii::app()->user->id;
			$inst->id_app = $id_app;
			$inst->id_app_user = $modelAppUser->id;
			$inst->old_value = $modelAppUser->list_size_bak;
			$inst->create_dta = date('Y-m-d H:i:s');
			$inst->status = ListsizeVerifications::STATUS_NEW;
			$inst->save(false);

			if ($modelAppUser->idApp->type == Apps::TYPE_AR_CUSTOM) {
				$code = self::RESULT_CODE_MANUAL;
			} else {
				$code = $inst->makeComplete(self::COMPLETE_RESULT_APPROVED) ?
					self::RESULT_CODE_AUTO : self::RESULT_CODE_ERROR;
			}
			$result[$code] = $code;
		}
		if (sizeof($result) == 1 && array_keys($result)[0] == self::RESULT_CODE_AUTO) {
			return self::RESULT_CODE_AUTO;
		} elseif (sizeof($result) == 1 && array_keys($result)[0] == self::RESULT_CODE_MANUAL) {
			return self::RESULT_CODE_MANUAL;
		} elseif (sizeof($result) == 1 && array_keys($result)[0] == self::RESULT_CODE_ERROR) {
			return self::RESULT_CODE_ERROR;
		} else {
			return self::RESULT_CODE_BOTH;
		}
	}

	public function makeComplete($complete_result, $value=false)
	{
		if ($complete_result == self::COMPLETE_RESULT_APPROVED) {
			$class = $this->idApp->class;

			$inst = new $class($this->id_user);
			if ($value) {
				$this->new_value = $value;
			} else {
				if (($size = $inst->getListSizeVerify())===false) {
					$this->status = self::STATUS_COMPLETE;
					$this->complete_result = self::COMPLETE_RESULT_CANCELED;
					$this->complete_dta = date('Y-m-d H:i:s');
					$this->save(false);
					return false;
				};
				$this->new_value  = $size;
			}

			$appsUsersModel = AppsUsers::model()->findByAttributes(array('id_user'=>$this->id_user, 'id_app'=>$this->id_app));
			$appsUsersModel->list_size = $appsUsersModel->list_size_bak = $this->new_value;
			$appsUsersModel->save(false);

			// автоматом опубликовать верифицированный лист
			$totalListSize = AppsUsers::getVerifiedListSize($this->id_user);
			if ($totalListSize > 0) {
				$this->idUser->is_list_verify=1;
				$this->idUser->list_size=$totalListSize;
				$this->idUser->save(false);
			}
		}
		$this->status = self::STATUS_COMPLETE;
		$this->complete_result = $complete_result;
		$this->complete_dta = date('Y-m-d H:i:s');
		$this->save(false);
		return true;
	}

	public function searchControl()
	{
		$criteria=new CDbCriteria;
		$criteria->with = array('idUser','idUser.usersOnline','idApp');
		$criteria->together=true;
		$criteria->order='t.status, t.create_dta DESC';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination' => array(
				'class' => 'LoadMorePagination',
				'reverse' => false,
				'pageSize' => 25,
			),
		));
	}
}
