<?php
class Ip2LocationSpec extends ActiveRecord
{
	public function tableName()
	{
		return 'ip2_location_spec';
	}

	public function rules()
	{
		return array(
			array('countrySHORT, countryLONG, ipREGION, ipCITY, ipLATITUDE, ipLONGITUDE', 'safe'),
			array('ip', 'required'),
			array('ip', 'IpV4'),
		);
	}

	protected function beforeSave()
	{
		if (parent::beforeSave()) {
			if ($this->isNewRecord) {
				$this->ipATON = ip2long($this->ip);
			}
			return true;
		} else {
			return false;
		}
	}

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public static function selectInfoLocation($ip)
	{
		$ip2LocationSpec = Ip2LocationSpec::model()->findByAttributes(array('ipATON' => ip2long($ip)));
		return empty($ip2LocationSpec) ? Ip2LocationSpec::model()->attributes : $ip2LocationSpec->attributes;
	}

	public static function weightedInfoLocation($ip)
	{
		$ip2LocationSpec = self::selectInfoLocation($ip);
		if ($ip2LocationSpec['id'] == null) {
			return Ip2Location::selectInfoLocation($ip);
		} else {
			return $ip2LocationSpec;
		}
	}
}
