<?php
class Country extends ActiveRecord
{
	const TOP = 5;
	const CACHE_COUNTRIES_DATA = 'cache-countries-data';
	const CACHE_COUNTRIES_DATA_DURATION = 86400; // 24*3600 = 86400

	const CACHE_CLICKS_DATA = 'cache-clicks-data';
	const CACHE_CLICKS_DATA_DURATION = 86400; // 24*3600 = 86400

	public $ord;

	public function tableName()
	{
		return 'country';
	}

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public static function getCcCountryOptions()
	{
		$list = CHtml::listData(self::model()->findAll(['order' => 'name']), 'abr', 'name');
		return array_merge([''=>'- Select country -'], $list);
	}

	public static function isValid($abr)
	{
		return self::model()->exists(['condition'=>'abr=:abr', 'params'=>[':abr'=>$abr]]);
	}

	public static function getModelByAbr($abr)
	{
		return self::model()->find(['condition'=>'abr=:abr', 'params'=>[':abr'=>$abr]]);
	}

	public static function getOptions()
	{
		$criteria = new CDbCriteria();
		$criteria->select = [
			"IF (abr IN ('".join("','" , ClicksStats::getTopTierCountries())."'), 1, 0) as ord",
			"t.*",
		];
		$criteria->order = 'ord DESC, name';
		return CHtml::listData(self::model()->findAll($criteria), 'id', 'name');
	}

	public static function getAll()
	{
		$res = [];
		$models = self::model()->findAll(['select'=>['id']]);
		foreach ($models as $model) $res[] = $model->id;
		return $res;
	}
}