<?php

class FileUpload extends CWidget
{
	public $id="fileUploader";
	public $css_class=false;
	public $config=array();
	public $callbacks=array();

	public function run()
	{
		if(empty($this->config['request']))
		{
			throw new CException('FineUploader: param "request" cannot be empty.');
		}

		$config = array(
			'multiple' => true,
			'request'=>array(
				'paramsInBody'=>true,
				'params'=>array('YII_CSRF_TOKEN'=>Yii::app()->request->csrfToken),
			),
			'text'=>array(
				'uploadButton' => '<i class="icon-plus icon-white"></i> Add file'
			),
			'template' => '<div class="qq-uploader span12">'.
				'<pre class="qq-upload-drop-area span12"><span>{dragZoneText}</span></pre>'.
				'<div class="qq-upload-button btn btn-success">{uploadButtonText}</div>'.
				'<span class="qq-drop-processing"><span>{dropProcessingText}</span><span class="qq-drop-processing-spinner"></span></span>'.
				'<ul class="qq-upload-list"></ul>'.
				'</div>',
			'classes' => array(
				'success' => 'alert alert-success',
				'fail' => 'alert alert-error',
			),
			'failedUploadTextDisplay' => array(
				'mode' => 'custom',
				'maxChars' => 60,
				'responseProperty' => 'error',
				'enableTooltip' => true,
			),
		);

		$config = $this->merge_array($config, $this->config);
		$config = CJavaScript::encode($config);

		$callbacks = '';
		if (!empty($this->callbacks)) {
			foreach ($this->callbacks as $callback => $function_item) $callbacks .= '.on("'.$callback.'", '.$function_item.')';
		}

		$selector = $this->id ? '"#'.$this->id.'"' : '".'.$this->css_class.'"';

		Yii::app()->getClientScript()->registerScript("fineuploader_".$this->id, '$('.$selector.').fineUploader('.$config.')'.$callbacks.';', CClientScript::POS_READY);

		$containerOptions = [];
		if ($this->css_class) {
			$containerOptions['class'] = $this->css_class;
		}
		if ($this->id) {
			$containerOptions['id'] = $this->id;
		}

		echo CHtml::tag('div', $containerOptions, '');
	}

	private function merge_array($arr_def, $arr_upd)
	{
		$out = is_array($arr_def) ? $arr_def : array();

		if (is_array($arr_upd)) {
			foreach ($arr_upd as $key => $value) {
				if (isset($arr_def[$key]) AND is_array($arr_upd[$key]))
					$out[$key] = $this->merge_array($arr_def[$key],$arr_upd[$key]);
				else
					$out[$key] = $value;
			}
		} else {
			$out = $arr_upd;
		}
		return $out;
	}
}