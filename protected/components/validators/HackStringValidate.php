<?php
class HackStringValidate extends CValidator
{
	protected function validateAttribute($object,$attribute)
	{
		if (is_array($object->$attribute)) {
			$object->$attribute = '';
		}
		if (!mb_check_encoding($object->$attribute, 'UTF-8'))
			$this->addError($object,$attribute,'Invalid input data');
	}
}