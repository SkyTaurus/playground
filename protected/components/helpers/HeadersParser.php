<?php

class HeadersParser
{
	public static $instance;

	public
		$url,
		$timeout = 5,
		$maxRedirects = 10,
		$includeInitialUrl = false,
		$saveCookie = true,
		$debugMode = false;

	protected
		$_rawHeaders = '',
		$_parsedHeaders = [],
		$_debug = [];

	public static function getLocations($url, $maxRedirects = null)
	{
		$inst = self::$instance = new self;
		$inst->url = $url;
		if ($maxRedirects) {
			$inst->maxRedirects = $maxRedirects;
		}
		$inst->query();
		if (
			$inst->_rawHeaders
			&& $inst->parseHeaders()
			&& $inst->testStatuses()
		) {
			return $inst->collectLocations();
		} else {
			return [];
		}
	}

	protected function query()
	{
		$ch = curl_init($this->url);
		curl_setopt($ch, CURLOPT_HEADER, 1);
		curl_setopt($ch, CURLOPT_NOBODY, true);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_TIMEOUT, $this->timeout);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $this->timeout);
		if ($this->maxRedirects) {
			curl_setopt($ch, CURLOPT_MAXREDIRS, $this->maxRedirects);
		}
		if ($this->saveCookie) {
			$cookiePath = Yii::getPathOfAlias('application.runtime') . '/' . md5($this->url) . time() . 'HeadersParserCookies.txt';
			curl_setopt($ch, CURLOPT_COOKIEJAR, $cookiePath);
			curl_setopt($ch, CURLOPT_COOKIEFILE, $cookiePath);
		}
		$this->_rawHeaders = curl_exec($ch);
		if($this->debugMode){
			$this->_debug['curl_error'] = curl_error($ch);
			$this->_debug['curl_errno'] = curl_errno($ch);
		}
		curl_close($ch);
		if (!empty($cookiePath) && file_exists($cookiePath)) {
			unlink($cookiePath);
		}
		return $this->_rawHeaders;
	}

	protected function parseHeaders()
	{
		$parsed = [];
		$chunks = explode("\r\n", $this->_rawHeaders);
		if (!$chunks || !is_array($chunks)) {
			return [];
		}
		foreach ($chunks as $chunk) {
			$parsedChunk = $this->splitHeaders($chunk);
			if (!$parsedChunk || !is_array($parsedChunk)) {
				continue;
			}
			$parsed[] = $this->normalizeHeaders($parsedChunk);
		}
		return $this->_parsedHeaders = $parsed;
	}

	protected function normalizeHeaders($headers)
	{
		$normalized = [];
		foreach ($headers as $k => $val) {
			$normalized[strtolower($k)] = $val;
		}
		return $normalized;
	}

	protected function splitHeaders($header)
	{
		$ret = [];
		$fields = explode("\r\n", preg_replace('/\x0D\x0A[\x09\x20]+/', ' ', $header));
		foreach ($fields as $field) {
			if (preg_match('/([^:]+): (.+)/m', $field, $match)) {
				preg_replace_callback(
					'/(?<=^|[\x09\x20\x2D])./', function ($matches) {
						return strtoupper($matches[0]);
					}, strtolower(trim($match[1]))
				);
				if (isset($ret[$match[1]])) {
					$ret[$match[1]] = [$ret[$match[1]], $match[2]];
				} else {
					$ret[$match[1]] = trim($match[2]);
				}
			}
		}
		return $ret;
	}

	protected function testStatuses()
	{
		foreach ($this->_parsedHeaders as $headers) {
			if (!empty($headers['status']) && !$this->validateHttpStatus($headers['status'])) {
				return false;
			}
		}
		return true;
	}

	protected function validateHttpStatus($statusHeader)
	{
		$status = substr($statusHeader, 9, 3);
		if (!is_numeric($status)) {
			return false;
		} elseif (strlen($status) < 3) {
			return false;
		} elseif (substr($status, 0, 1) != 2 and substr($status, 0, 1) != 3) {
			return false;
		}
		return true;
	}

	protected function collectLocations()
	{
		// save start scheme and host
		$scheme = $host = $path = null;
		if ($parse = parse_url($this->url)) {
			if (!empty($parse['scheme'])) $scheme = $parse['scheme'];
			if (!empty($parse['host'])) $host = $parse['host'];
			if (!empty($parse['path'])) $path = dirname($parse['path']);
		}

		$locations = [];
		foreach ($this->_parsedHeaders as $headers) {
			if (!empty($headers['location'])) {

//				echo "{$headers['location']}<br>";

				// get current location scheme and host
				$locScheme = $locHost = $locPath = null;
				if ($parse = parse_url($headers['location'])) {
					if (!empty($parse['scheme'])) $locScheme = $parse['scheme'];
					if (!empty($parse['host'])) $locHost = $parse['host'];
					if (!empty($parse['path'])) $locPath = dirname($parse['path']);
				}

				// convert locations from local to global URLs
				if ((!$locScheme || !$locHost) && $scheme && $host) {
					$location = "{$scheme}://{$host}";
					if (substr($headers['location'], 0, 1)=='/') {
						$headers['location'] = "{$scheme}://{$host}{$headers['location']}";
					} else {
						$path .= '/';
						$headers['location'] = "{$scheme}://{$host}{$path}{$headers['location']}";
					}
				}

				// save current scheme and host as global
				if ($locScheme) $scheme = $locScheme;
				if ($locHost) $host = $locHost;
				if ($locPath) $path = $locPath;

				$locations[] = $headers['location'];
			}
		}
		if ($this->includeInitialUrl) {
			array_unshift($locations, $this->url);
		}

		return $locations;
	}

	public function debug()
	{
		var_dump($this->_debug);
	}
}