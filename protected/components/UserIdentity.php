<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
	private $_id;
	private $userModel;

	/**
	 * Не сохраняем токен в куку если логин происходит не через веб (н-р через моб. API)
	 *
	 * @var bool
	 */
	public $skipLoginToken = false;

	public function authenticate()
	{
		$this->userModel=Users::model()->find('LOWER(email)=? AND role != ?', array(strtolower($this->username), Users::ROLE_DELETED));

		if($this->userModel===null)
			$this->errorCode=self::ERROR_USERNAME_INVALID;
		else if(!$this->userModel->validatePassword($this->password)) {
			$this->errorCode=self::ERROR_PASSWORD_INVALID;
		} else {
			$this->_id=$this->userModel->id;
			if (empty($this->skipLoginToken)) {
				$this->saveLoginToken();
			}
			$this->errorCode=self::ERROR_NONE;
		}

		return $this->errorCode==self::ERROR_NONE;
	}

	/**
	 * Сохранение ключа безопасности для логина по кукам
	 */
	public function saveLoginToken($userModel = null)
	{
		$this->setState('saved_key', UserIdentityKeys::create($userModel ?: $this->userModel, 'cookie_login', true, true));

	}

	// Используется после регистрации.
	public function setId($id)
	{
		$this->_id = $id;
	}

	public function getId()
	{
		return $this->_id;
	}

	/**
	 * @return mixed
	 */
	public function getUserModel()
	{
		return $this->userModel;
	}

	public function setUserModel($model)
	{
		$this->userModel = $model;
	}
}