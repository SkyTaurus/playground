<?= CHtml::openTag('div', $this->htmlOptions) ?>
	<input type="hidden" name="<?= $this->name ?>" value="<?= $this->select ?>">
	<div class="e-lbl js-lbl"><?= $selectedLabel ?><?php if (!$this->hideCaret): ?><i class="fa fa-caret-down"></i><?php endif ?></div>
	<ul class="b-dropmenu js-dropmenu<?= $this->dropPosition == 'right' ? ' m-right' : '' ?>">
		<?php foreach($this->data as $k=>$v): ?>
			<li data-val="<?= $k ?>"><?= $v ?></li>
		<?php endforeach ?>
	</ul>
<?= CHtml::closeTag('div') ?>