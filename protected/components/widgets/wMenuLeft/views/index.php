<?php $this->controller->jsInit('appWidgetMenuLeft', 'init') ?>
<div class="app-widgets-menu-left js-app-widgets-menu-left js-appmain-menu-depend-marker">
	<ul>
		<?php foreach($items as $item): ?>
			<?php if ($item->attributes['visible']): ?>
				<?= CHtml::openTag('li', ['class'=>$item->attributes['liClass']]) ?>

					<?php if ($item->attributes['isDenied']): ?>

						<div class="denied-menu-item js-denied-menu-item">
							<span class="e-lbl"><?= $item->attributes['label'] ?></span>
							<?php if ($item->attributes['iconDenied']): ?>
								<?= $item->attributes['iconDenied'] ?>
							<?php endif ?>
						</div>

					<?php elseif ($item->attributes['url']): ?>

						<?= CHtml::openTag('a', array_merge($item->attributes['htmlOptions'], ['href'=>$item->attributes['url']])) ?>
							<span class="e-lbl"><?= $item->attributes['label'] ?></span>

							<?php if ($item->attributes['icon']): ?>
								<?= $item->attributes['icon'] ?>
							<?php elseif ($item->attributes['cntPrimary']>0): ?>
								<span class="badge-cnt">+<?= $item->attributes['cntPrimaryFormated'] ?></span>
							<?php elseif ($item->attributes['cntSlave']>0): ?>
								<span class="badge-cnt-slave"><?= $item->attributes['cntSlaveFormated'] ?></span>
							<?php endif ?>
						<?= CHtml::closeTag('a') ?>

					<?php else: ?>
						<div>
							<?= $item->attributes['label'] ?>
						</div>
					<?php endif ?>

				<?= CHtml::closeTag('li') ?>
			<?php endif ?>
		<?php endforeach ?>
	</ul>
</div>