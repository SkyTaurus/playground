<?php
class wFullname extends Widget
{
	/**
	 * @var модель юзера либо его Id
	 */
	public $model;

	/**
	 * @var ссылка на fullname
	 */
	public $url;

	public $urlHtmlOptions = array();

	// icons left side
	public $is_showAwardIcon = true;
	// icons right side
	public $is_showSubscriptionActiveIcon = false;
	public $is_showListVerifiedIcon = false;
	public $is_showFastRespondIcon = false;
	public $is_showFireIcon = false;
	public $iconsOff = false;

	public $iconsWrapHtmlOptions = array();

	public $is_multiline = false;

	public $htmlOptions = array();

	public $wrapHtmlOptions = array();

	public $tagWrapper = 'span';

	public $onlyFname = false;

	public function init()
	{
		if (!$this->model instanceof ActiveRecord) {
			if (!is_numeric($this->model)) {
				$class = get_class($this->model);
				$this->model = $class::model()->findByPk($this->model);
			} else {
				$this->model = Users::model()->findByPk($this->model);
			}
			if (empty($this->model)) {
				throw new CException('User not found');
			}
		}

		if (!isset($this->urlHtmlOptions['data-scrollto'])) {
			$this->urlHtmlOptions['data-scrollto'] = 'top';
		}

		$containerCssClass = 'app-widgets-fullname';
		$this->containerCssClass = empty($this->containerCssClass) ? $containerCssClass : " ".$containerCssClass;

		// Обязательный вызов.
		parent::init(__CLASS__);
	}

	/**
	 * @return string|void Классический метод виджета
	 */
	public function run()
	{
		$html = empty($this->wrapHtmlOptions) ? '' : CHtml::openTag('div', $this->wrapHtmlOptions);


		if ($this->is_showAwardIcon && !$this->iconsOff) {
			$html .= $this->model->is_award ? $this->wrapLink('<i class="icon-medal"></i>').' ':'';
		}

		if ($this->onlyFname && $this->model->fullname) {
			$nameArr = explode(' ', $this->model->fullname);
			$fullname = !empty($nameArr[0]) ? trim($nameArr[0]) : $this->model->fullname;
		} else {
			$fullname = $this->model->fullname;
		}

		$fullname = $this->is_multiline ? preg_replace("/\s+/", '<br>', $fullname) : $fullname;
		$html .= $this->wrapLink(CHtml::tag('span', $this->htmlOptions, $fullname));

		// right icons
		$icons = [];
		if ($this->is_showSubscriptionActiveIcon) {
			$icons[] = '<i class="help-hint-default fa fa-star" data-placement="top" data-container="body" data-trigger="hover" data-content="Prime member of Udimi"></i>';
		}
		if ($this->is_showListVerifiedIcon) {
			$icons[] = '<i class="help-hint-default icon-verified-small" data-placement="top" data-container="body" data-trigger="hover" data-content="List size of this person has been manually verified by Udimi team.<br>To verify your own list, go to Settings > Seller setup"></i>';
		}
		if ($this->is_showFastRespondIcon) {
			$icons[] = '<i data-content="This seller accepts orders fast." data-trigger="hover" data-container="body" data-placement="top" class="help-hint-default icon icon-fast-accept-envelope accepts-fast" data-original-title=""></i>';
		}
		if ($this->is_showFireIcon) {
			$icons[] = '<i class="help-hint-default glyphicon glyphicon-fire" data-placement="top" data-container="body" data-trigger="hover" data-content="This seller sold more than '.(ProfileSolo::HOT_SALE_MINIMUM-1).' solos during last week"></i>';
		}
		if (!empty($icons) && !$this->iconsOff) {
			$html .= CHtml::tag('span', $this->iconsWrapHtmlOptions, implode('', $icons));
		}

		if (!empty($this->wrapHtmlOptions)) $html .= CHtml::closeTag('div');

		// Обязательно указываем return, чтобы виджет работал как при обычной загрузке страницы, так и через аякс
		return $this->render(array('html'=>$html));
	}

	private function wrapLink($html)
	{
		if ($this->url !== null) {
			$html = CHtml::link($html, $this->url, $this->urlHtmlOptions);
		}
		return $html;
	}
}