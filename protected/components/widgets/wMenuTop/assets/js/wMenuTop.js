function AppWidgetsMenuTop() {

	this.options = {};
	this.lockedDrop = {};
	var self = this;

	this.init = function()
	{
		$('.js-mt-child').parents('li').mouseenter(function() {
			var $child = $(this).find('.js-mt-child');
			if (typeof self.lockedDrop[$child.data('name')] != 'undefined') {
				return;
			}
			self.showChild($child);
		});

		$('.js-mt-child').parents('li').mouseleave(function() {
			self.hideChild($(this).find('.js-mt-child'));
		});

		$('.js-mt-child').click(function() {
			self.hideChild($(this));
		});
	}

	this.showChild = function($drop)
	{
		$drop.removeClass('hidden');
	}

	this.hideChild = function($drop)
	{
		setTimeout(function () {$drop.addClass('hidden')}, 500);
	}
}
var appWidgetsMenuTop = new AppWidgetsMenuTop();
