<?php

class wTabsGeneral extends Widget
{
	public $items = array();
	public $containerClass = '';

	public function init()
	{
		// Обязательный вызов.
		parent::init(__CLASS__);
	}

	/**
	 * @return string|void Классический метод виджета
	 */
	public function run()
	{
		$li = '';
		foreach ($this->items as $item) {
			if (empty($item['itemOptions']))
				$item['itemOptions'] = array();
			if (empty($item['linkOptions']))
				$item['linkOptions'] = array();

			if (!empty($item['active'])) {
				$item['itemOptions']['class'] = empty($item['itemOptions']['class']) ? 'active' : $item['itemOptions']['class'].' active';
			}

			if (isset($item['visible']) && $item['visible'] == false) {
				continue;
			}

			$li .= CHtml::openTag('li', $item['itemOptions']);
			if (isset($item['linkOptions']['class']) && strpos($item['linkOptions']['class'], 'ajax-get-modal') !== false) {
				$li .= CHtml::tag(
					'span',
					CMap::mergeArray($item['linkOptions'], array('data-href'=>$item['url'])),
					$item['label']
				);
			} elseif (isset($item['url'])) {
				$li .= CHtml::link($item['label'], $item['url'], $item['linkOptions']);
			} elseif (!empty($item['raw_html'])) {
				$li .= $item['raw_html'];
			} else {
				$li .= CHtml::link($item['label'], '', $item['linkOptions']);
			}
			$li .= CHtml::closeTag('li');
		};
		// Обязательно указываем return, чтобы виджет работал как при обычной загрузке страницы, так и через аякс
		return $this->render('index', array('li'=>$li));
	}

	public static function actions()
	{
		Yii::setPathOfAlias(__CLASS__, realpath(dirname(__FILE__)));
		return array(
//			'reload'=>array('class'=>'application.components.WidgetBaseAction', 'widget_alias'=>__CLASS__),
		);
	}
} 