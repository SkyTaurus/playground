<div class="app-widgets-gtitle">
	<table class="b-tbl">
		<tr>
			<td class="td-col-1" nowrap="nowrap">
				<?= $this->left ?>
			</td>
			<td class="td-col-2">
				<div class="e-line"></div>
			</td>
			<?php if ($this->right): ?>
				<td class="td-col-3" nowrap="nowrap">
					<?= $this->right ?>
				</td>
			<?php endif ?>
		</tr>
	</table>
</div>