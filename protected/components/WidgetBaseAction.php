<?php
class WidgetBaseAction extends CAction
{
	/**
	 * @var path alias of widget class. Ex.: application.components.widgets.wFeedBack.wFeedBack
	 * Заполняется при регистрации экшна в виджете. Используется для создания экземпляра виджета.
	 */
	public $widget_alias;

	/**
	 * @var instance of widget
	 */
	protected $widget;

	protected function getWidgetInstance()
	{
		if ($this->widget === null) {
			$params = isset($_GET['widgetparams']) ? $_GET['widgetparams'] : array();
			$params['returnHtml'] = true;
			$this->widget = $this->controller->createWidget(
				$this->widget_alias,
				$params
			);
		}
		return $this->widget;
	}

	/**
	 * Перерисовка виджета
	 */
	public function run()
	{
		$widget = $this->getWidgetInstance();

        $params = [
            'container' => '#' . $widget->containerId,
            'content' => $widget->run(),
        ];
        if (!empty($widget->callbacks)) {
            $params['callback'] = implode(';', $widget->callbacks);
        }
        // Response
        $this->controller->jsonResponse($params);
	}

	public function getModuleViewDirAlias()
	{
		return 'application.modules.'.$this->controller->getModule()->id.'.components.widgets.'.$this->widget_alias.'.views';
	}
}