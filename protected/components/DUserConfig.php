<?php
class DUserConfig extends CApplicationComponent
{
	public $id_user;
	private $_data;

	public function getData($refresh=false)
	{
		if ($this->_data === null || $refresh) {
			$this->_data = array();
			$models = UserConfig::model()->findAllByAttributes(array('id_user'=>$this->id_user));
			if (!empty($models)) {
				foreach($models as $model) {
					$this->_data[$model['param']] = $model['value'];
				}
			}
		}
		return $this->_data;
	}

	public function get($key, $return=null)
	{
		if (array_key_exists($key, $this->data)) {
			return $this->data[$key];
		}
		else {
			return $return;
		}
	}

	public function set($key, $value)
	{
		if (empty($this->id_user)) {
			throw new CException('Undefined id user');
		}
		if (array_key_exists($key, $this->data)) {
			$model = UserConfig::model()->findByAttributes(array('param'=>$key, 'id_user'=>$this->id_user));
			if (!$model)
				throw new CException('Undefined parameter ' . $key);

			$model->value = $value;
			$model->save(false);
		} else {
			$model = new UserConfig();
			$model->param = $key;
			$model->value = $value;
			$model->id_user = $this->id_user;
			$model->save(false);
		}

		$this->getData(true);
	}

	public function delete($key)
	{
		if (array_key_exists($key, $this->data)) {
			if (empty($this->id_user)) {
				throw new CException('Undefined id user');
			}
			$model = UserConfig::model()->findByAttributes(array('param'=>$key, 'id_user'=>$this->id_user));
			if (!$model)
				throw new CException('Undefined parameter ' . $key);

			$model->delete();
		}
	}
}