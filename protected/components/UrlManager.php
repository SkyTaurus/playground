<?php
class UrlManager extends CUrlManager
{
	public $modulesWithUrlRules = array();

	public function init()
	{
		parent::init();

		// add dynamic rules from modules
		if (!is_array($this->modulesWithUrlRules))
			throw new CException('Invalid type of class property');
		foreach ($this->modulesWithUrlRules as $moduleName) {
			$this->applyUrlRules($moduleName);
		}
	}

	public function createUrl($route, $params=array(), $ampersand='&')
	{
		$url = parent::createUrl($route, $params, $ampersand);
		return MultilangHelper::addLangToUrl($url);
	}

	private function applyUrlRules($moduleName)
	{
		if(Yii::app()->hasModule($moduleName))
		{
			$class = ucfirst($moduleName) . 'Module';
			Yii::import($moduleName . '.' . $class);

			if(method_exists($class, 'rules'))
			{
				$rules = call_user_func($class .'::rules');
				$this->addRules($rules['items'], isset($rules['append']) ? $rules['append'] : false);
			}
		}
	}
}