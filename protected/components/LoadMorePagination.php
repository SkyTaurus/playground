<?php
class LoadMorePagination extends CPagination
{
	/**
	 * @var bool Реверсная догрузка данных. Используется при догрузки истории в обратном порядке.
	 * Сначала будет загружаться последняя страничка постранички. Причем ее размер будет равен pageSize.
	 * Как только догрузка в реверсном режиме доберется до последней порции данных,
	 * (а это в реальности будет первая страница постранички) то ее размер уже может либо равняться pageSize
	 * либо быть меньше (смотря сколько осталось дынных)
	 */
	public $reverse = false;

	/**
	 * @var string Имя переменной GET которая своим присутствием помечает запрос как запрос на дозагрузку данных.
	 */
	public $isLoadMoreVar = 'loadmore';

	/**
	 * @var int size of the first page
	 */
	public $firstPageSize;
	/**
	 * @var int size of "lastPageSize" records. They are not used in pagination and itemCount
	 */
	public $lastPageSize = 0;

	/**
	 * @var boolen - if set to true, all records from start till current page are loaded
	 */
	public $loadAllPages = false;

	/**
	 * @var string Всегда выводить кнопку догрузки, даже если датапровадер содержит одну страницу.
	 */
	public $alwaysShowLoadMoreLabel = false;

	/**
	 * @var string ссылка load more будет содержать вот такой innerHtml:
	 */
	public $loadMoreLabelTemplate = 'Load {per_page} out of {n} collapsed item|Expand {per_page} out of {n} collapsed items';

	/**
	 * @return integer the offset of the data. This may be used to set the
	 * OFFSET value for a SQL statement for fetching the current page of data.
	 */
	public function getOffset()
	{
		$page = $this->getCurrentPage();

		if ($this->reverse == false) {
			// if this is the first page or loadAllPages mode, offset will be zero
			// otherwise, calculate it using firstPageSize
			$offset = $this->loadAllPages || !$page ? 0 :
				$this->getFirstPageSize() + ($page-1)*$this->getPageSize(true);
		} else {
			// calculate offset from the end, using current page number
			$offset = $this->getItemCount() - $this->getFirstPageSize() - ($this->getPageCount()-$page-1)*$this->getPageSize(true);
			// add lastPageSize, b/c it's subtracted from getItemCount
			$offset += $this->lastPageSize;
			// adding shift only if this is "the last page" (the one before lastPageSize). THis way we should not have negative values
			if ($page==0) $offset += $this->calculateShift();

//			$offset = ($offset < 0) ? 0 : $offset;
		}

		return $offset;

	}

	/**
	 * @return integer the limit of the data. This may be used to set the
	 * LIMIT value for a SQL statement for fetching the current page of data.
	 * This returns the same value as {@link pageSize}.
	 */
	public function getLimit()
	{
		$page = $this->getCurrentPage();

		if ($this->reverse == false) {
			if ($page == $this->getPageCount()-1) {
				// for last page we should return sharp quantity of records,
				// b/c lastPageSize may exist and it should not be taken
				$limit = $this->getItemCount() - $this->getOffset();
			} else {
				// else return regular pageSize
				$limit = $this->getPageSize();
				// if loadAllPages mode, add everything from zero b/c offset is 0
				if ($this->loadAllPages && $page)
					$limit += $this->getFirstPageSize() + ($page-1)*$this->getPageSize(true);
			}

		} else {
			if ($this->loadAllPages) {
				// loadAllPages mode, load everything from offset (but without lastPageSize inside offset)
				$limit = $this->getItemCount() - $this->offset + $this->lastPageSize;
			} elseif ($page==0) {
				// it's last page for reverse mode, calculate sharp size
				$limit = $this->getItemCount() - $this->getPageSize(true)*($this->getPageCount()-2) - $this->getFirstPageSize();
			} else {
				// or usual page size
				$limit = $this->getPageSize();
			}
		}

		return $limit;

	}


	/**
	 * @return int Величина сдвига для реверсного режима догрузки.
	 * Цель - обеспечить последнюю страничку постранички полной (равной pageSize),
	 * а первую страничку - то что осталось (меньше либо равно pageSize)
	 */
	public function calculateShift()
	{
		return ($this->pageCount-1)*$this->getPageSize(true)+$this->getFirstPageSize() - $this->itemCount;
	}

	/**
	 * @return bool|string URL отправляется в серверном ответе. JS виджет получает его
	 * и использует его в качестве нового значения href ссылки loadmore.
	 */
	public function getNextPageUrl()
	{
		if ($this->reverse == false) {
			if ($this->getCurrentPage() >= $this->getPageCount()-1) {
				return false;
			} else {
				return $this->createPageUrl(Yii::app()->getController(), $this->getCurrentPage()+1, true);
			}
		} else {
			if ($this->getCurrentPage() == 0) {
				return false;
			} else {
				return $this->createPageUrl(Yii::app()->getController(), $this->getCurrentPage()-1, true);
			}
		}
	}

	/**
	 * @return string URL используется виджетом для подстановки
	 * первоначального значения href ссылки loadmore в момент выполнения кода виджета в потоке рендеринга страницы.
	 */
	public function getCurrentPageUrl()
	{
		if ($this->reverse == false) {
			$pageCurrent = $this->getCurrentPage();
			return $this->createPageUrl(Yii::app()->getController(), $pageCurrent, true);
		} else {
			$pageCount = $this->getPageCount();
			return $this->createPageUrl(Yii::app()->getController(), $pageCount-1, true);
		}
	}

	/**
	 * Перепишем метод createPageUrl и будем примешивать в get признак того что запрос относится к дозагрузке данных
	 * @param CController $controller
	 * @param int $page
	 * @param bool $addLoadMore - добавлять\нет get параметр loadmore=1. Для wLinkPager нет.
	 *
	 * @return string
	 */
	public function createPageUrl($controller, $page, $addLoadMore = false)
	{
		$params=$this->params===null ? $_GET : $this->params;
		if (($page>0 && !$this->reverse) || ($page<$this->getPageCount() && $this->reverse)) // page 0 is the default
			$params[$this->pageVar]=$page+1;
		else
			unset($params[$this->pageVar]);

		if ($addLoadMore)
			$params[$this->isLoadMoreVar]=1;
		else if (!$addLoadMore AND isset($params['loadmore']))
			unset($params['loadmore']);

		return $controller->createUrl($this->route,$params);
	}

	/**
	 * if firstPageSize is not set or zero, it returns regular pageSize
	 * @return int
	 */
	public function getFirstPageSize()
	{
		return $this->firstPageSize === null ? parent::getPageSize() : $this->firstPageSize;
//		return $this->firstPageSize ? $this->firstPageSize : parent::getPageSize();
	}

	/*
	 * getPageCount calculated different way, b/c the size of firstPage can differ from size of regular pages
	 */
	public function getPageCount()
	{
		if ($this->getItemCount(true)== 0) return 0;

		$itemCount = $this->getItemCount();
		$pageSize = $this->getPageSize(true);
		$pageCount = (int)(($itemCount - $this->getFirstPageSize() + $pageSize-1)/$pageSize) + 1;
		if ($pageCount <=0) $pageCount = 1;
		return $pageCount;
	}

	/**
	 * We should not use lastPageSize size in itemCount to calculate pageCount correctly
	 * If real item count wanted (with lastPageSize), pass default=true parameter
	 * @param bool $default
	 * @return int
	 */
	public function getItemCount($default = false)
	{
		if ($default) {
			return parent::getItemCount();
		} else {
			return parent::getItemCount() > $this->lastPageSize ? parent::getItemCount()-$this->lastPageSize : 0;
		}
	}

	/**
	 * Page size return different values depending on current page value
	 * Pass default=true parameter to get regular pageSize
	 * @param bool $default
	 * @return int
	 */
	public function getPageSize($default = false)
	{
		// get defauilt page size without referring getPageCount
		if ($default) return parent::getPageSize();

		// calculate using getPageCount
		$currentPage = $this->getCurrentPage();

		if ($currentPage==0 && !$this->reverse) return $this->getFirstPageSize();
		elseif ($currentPage==$this->getPageCount()-1 && $this->reverse) return $this->getFirstPageSize();
		else return parent::getPageSize();
	}

	/**
	 * Create dataProvider for lastPageSize records if exists
	 * @param $dataProvider
	 * @return bool|CActiveDataProvider
	 */
	public static function searchLastPage($dataProvider)
	{
		$pagination = $dataProvider->pagination;
		if (!$pagination || !$pagination->lastPageSize) return false;

		$itemCount = $pagination->getItemCount(true);
		$criteria = $dataProvider->criteria;

		if (!$pagination->reverse) {
			$limit = $pagination->lastPageSize < $itemCount ? $pagination->lastPageSize : $itemCount;
			$criteria->offset = $limit ? $itemCount - $limit : 0;;
			$criteria->limit = $limit;
		} else {
			$limit = $pagination->lastPageSize < $itemCount ? $pagination->lastPageSize : $itemCount;
			$criteria->offset = 0;;
			$criteria->limit = $limit;

		}

		return new CActiveDataProvider($dataProvider->modelClass, array(
			'criteria' => $criteria,
			'pagination'=>false,
		));
	}


	/**
	 * @param boolean $recalculate whether to recalculate the current page based on the page size and item count.
	 * @return integer the zero-based index of the current page. Defaults to 0.
	 */
	public function getCurrentPage($recalculate=true)
	{

		if (!isset($_GET[$this->pageVar]) && !$this->reverse) $currentPage = 0;
		elseif (!isset($_GET[$this->pageVar]) && $this->reverse) $currentPage = $this->getPageCount()-1;
		else $currentPage = parent::getCurrentPage($recalculate);
		return $currentPage;

	}

	public function getLoadedPages()
	{
		if ($this->reverse) {
			return $this->getPageCount() - $this->getCurrentPage();
		} else {
			return $this->getCurrentPage();
		}
	}

	/**
	 * @return int Кол-во оставшихся на догрузку айтемов.
	 */
	public function getCollapsed()
	{
		// 15 - (0 + 5 + 0-1*5);
		$minusOne = $this->reverse ? -1 : 0;
		$collapsed = $this->getItemCount(true) - ($this->lastPageSize + $this->getFirstPageSize() + ($this->getLoadedPages()+$minusOne)*$this->getPageSize(true));

		return $collapsed <= 0 ? 0 : $collapsed;
	}

	public function getLoadMoreLabel()
	{
		$collapsed = $this->getCollapsed();
		$per_page = $this->getPageSize(true);

		// когда дело доходит до неполной последней странице, то per_page должен стать равен остатку (т.е. текущему $collapsed).
		$per_page = $collapsed < $per_page ? $collapsed : $per_page;

		$template = Yii::t('app', $this->loadMoreLabelTemplate, $collapsed);

		return str_replace('{per_page}', $per_page, $template);
	}

	public static function isLoadMore()
	{
		$isGet = Yii::app()->request->getQuery('loadmore', false);
		return $isGet && Yii::app()->request->isAjaxRequest;
	}
}