<?php
class Save extends WidgetBaseAction
{
	public function run()
	{
		if (isset($_POST['Users'])) {
			$widget = $this->getWidgetInstance();
			$model = $widget->model;
			$model->attributes = $_POST['Users'];

			if ($model->validate()) {
				$model->save();
			}  else {
				$this->controller->jsonResponse(array('error'=>MyUtils::getFirstError($model)));
			}

			// создадим еще раз экземпляр пустой модели и выведем пустую форму
			$widget->init();

			// Response
			$this->controller->jsonResponse(
				array(
					'callback' => [
						'appMain.showToast("Password changed", "success")',
						'appMain.closeModal()'
					],
					'container' => '#'.$widget->containerId,
					'content' => $widget->run(),
				)
			);

		}
	}
}