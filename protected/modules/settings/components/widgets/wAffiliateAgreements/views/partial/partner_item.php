<?php if ($user): ?>
	<div class="item-wrap">

		<?php if (!empty($uid)): ?>
			<span data-ok="1" data-href="<?=$this->createUrl('waffiliateagreements.cancel', ['uid'=>$uid]) ?>" data-widget="waffiliateagreements" data-confirm="Are you sure to cancel agreement?" class="e-cancel ajax-post">cancel</span>
		<?php endif ?>

		<div class="item-wrap-avatar">
			<div class="item-avatar-col">
				<?php $this->widget('w.wAvatar.wAvatar', [
					'user' => $user,
					'htmlOptions' => [
						'width' => 45,
						'class'=>'avatar',
					],
				]) ?>
			</div>
		</div>

		<div class="item-wrap-percent">
			<?php if (!empty($percent)): ?>
				<div class="b-agreement-percent">
					<?=$percent ?>%
				</div>
				<div class="b-agreement-copy">
					off the order total
				</div>
			<?php endif ?>
		</div>

		<div class="item-wrap-body">
			<div class="b-fullname">
				<?php $this->widget('w.wFullname.wFullname', array(
					'model'=>$user,
					'is_showAwardIcon'=>false,
					'is_showSubscriptionActiveIcon'=>$user->idSubscriptionActive,
					'is_showListVerifiedIcon'=>$user->is_list_verify,
					'is_showFastRespondIcon'=>$user->isFastRespond(),
				)) ?>
			</div>

			<?php if (!empty($uid)): ?>
				<div class="b-agreement-link">
					<?php $scheme = empty(Yii::app()->params['is_local']) ? 'https' : 'http' ?>
					<span class="lbl">Use link:</span> <?= $this->createAbsoluteUrl('/profile/general/index', ['sellerUid'=>Yii::app()->user->model->uid, 'kentUid'=>$user->uid], $scheme) ?>
				</div>
			<?php endif ?>

		</div>

	</div>
<?php else: ?>
	<div class="item-wrap-empty">
		<p>Paste your partner's profile URL above</p>
		<span class="ajax-loader-progress icon-ajax-loader" style="display: none"></span>
	</div>
<?php endif ?>
