<?php $this->jsInit('settingsPrivacy', 'init') ?>

<?php $this->beginContent('/layouts/main') ?>
<div class="settings-privacy-index">

	<?php $form = $this->beginWidget('CActiveForm', [
		'htmlOptions' => ['class' => 'form-horizontal']
	]) ?>

	<div class="b-row">
		<div class="checkbox checkbox-primary">
			<?= $form->checkBox($model, 'privacy_hide_news_buy', [
				'id' => 'privacy_hide_news_buy',
				'value' => 1,
				'class' => 'privacy-checkbox',
			]) ?>
			<label class="checkbox-skip-color" for="privacy_hide_news_buy"><?= $model->getAttributeLabel('privacy_hide_news_buy'); ?></label>
		</div>
	</div>

	<div class="b-row">
		<div class="checkbox checkbox-primary">
			<?= $form->checkBox($model, 'privacy_hide_news_rate', [
				'id' => 'privacy_hide_news_rate',
				'value' => 1,
				'class' => 'privacy-checkbox',
			]) ?>
			<label class="checkbox-skip-color" for="privacy_hide_news_rate"><?= $model->getAttributeLabel('privacy_hide_news_rate'); ?></label>
		</div>
	</div>

	<div class="b-row">
		<div class="checkbox checkbox-primary">
			<?= $form->checkBox($model, 'privacy_hide_mas', [
				'id' => 'privacy_hide_mas',
				'value' => 1,
				'class' => 'privacy-checkbox',
			]) ?>
			<label class="checkbox-skip-color" for="privacy_hide_mas"><?= $model->getAttributeLabel('privacy_hide_mas'); ?></label>
		</div>
	</div>

	<button type="submit" class="ajax-post hidden js-privacy-submit"></button>
	<?php $this->endWidget() ?>



</div>
<?php $this->endContent() ?>