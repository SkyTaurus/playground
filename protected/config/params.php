<?php
return array(
	'resourcesVersionFilename' => 'res.version',
	'default_time_zone' => 'UTC',
	'defaultPageSize' => 25,
	'chmode_folder' => 777,
	'chmode_file' => 644,
	'allowedExtensions' => array('gif', 'jpe', 'jpeg', 'jpg', 'png', 'qif', 'bmp'),
	'companyName' => 'Udimi',
	'systemEmailAddress' => 'udimi@udimi.com',
	'abuseEmailAddress' => 'abuse@udimi.com',
	'translatedLanguages'=>array(
		'en'=>'English',
		'ru'=>'Русский',
	),
	'defaultLanguage'=>'en',
);