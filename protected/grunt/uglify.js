module.exports = {
	all: {
		files: [{
			expand: true,
			cwd: '../httpdocs/media/js-dev',
			src: ['*.js'],
			dest: '../httpdocs/media/js',
			ext: '.min.js'
		}]
	}
};