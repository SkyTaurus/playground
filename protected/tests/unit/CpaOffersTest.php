<?php
class CpaOffersTest extends CDbTestCase
{
	protected $offers;

	protected function setUp()
	{
		parent::setUp();
		$this->offers = new CpaOffers();
	}

	public function testNameIsRequired()
	{
		$this->offers->scenario = 'create';
		$this->offers->name = '';
		$this->assertFalse($this->offers->validate(['name']));
	}

}